from django import template
from django.core.urlresolvers import resolve

register = template.Library()


@register.simple_tag()
def nav_active(request, app):
    url_name = resolve(request.path).app_name

    if url_name == app:
        return 'class="selected"'
    else:
        return ''
