from django.contrib import admin
from blog.models import Article, Category, Comment


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'date', 'visible', 'enable_comments', 'category')
    list_filter = ('category', )
    date_hierarchy = 'date'
    ordering = ('-date', )
    search_fields = ('title', )
    prepopulated_fields = {'slug': ('title', )}

    class Media:
        css = {
            'all': ('css/redactor.css',)
        }

        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js',
            'js/redactor.js',
            'js/redactor_init.js',
        )


class CommentAdmin(admin.ModelAdmin):
    list_display = ('author', 'email', 'get_content', 'article')
    list_filter = ('article', )
    ordering = ('-date', )
    search_fields = ('content', 'author')

    def get_content(self, comment):
        cont = comment.content[0:40]

        if len(cont) > 40:
            return '%s...' % cont
        else:
            return cont

    get_content.short_description = 'Commentaire'


# Register your models here.
admin.site.register(Article, ArticleAdmin)
admin.site.register(Category)
admin.site.register(Comment, CommentAdmin)
