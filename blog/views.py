from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Article, Comment
from django.views.generic import ListView
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage
from blog.forms import AddCommentForm


class ListArticles(ListView):
    """Generic view - shows last articles"""

    model = Article
    context_object_name = 'articles'
    template_name = 'blog/index.html'
    paginate_by = 3
    queryset = Article.objects.filter(visible=True).order_by('-date')


def view_article(request, id, slug, page=1):
    article = get_object_or_404(Article, id=id, slug=slug)
    comment_list = Comment.objects.filter(article=article).order_by('-date')
    pg = Paginator(comment_list, 8)

    if request.method == 'POST':
        form = AddCommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.article = article
            comment.save()

            return redirect(reverse('blog:blog_article', args=(id, slug, )) + '#comment' + str(comment.pk))
    else:
        form = AddCommentForm()

    try:
        comments = pg.page(page)
    except EmptyPage:
        comments = pg.page(pg.num_pages)

    context = {
        'article': article,
        'form': form,
        'comments': comments
    }

    return render(request, 'blog/article_view.html', context)


