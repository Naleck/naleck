from django.conf.urls import patterns, url
from blog.views import ListArticles

urlpatterns = patterns(
    'blog.views',
    url(r'^$', ListArticles.as_view(), name="blog_index"),
    url(r'^(?P<page>\d+)$', ListArticles.as_view(), name="blog_index"),
    url(r'^article/(?P<id>\d+)-(?P<slug>[a-z0-9\-]+)$', 'view_article', name="blog_article"),
    url(r'^article/(?P<id>\d+)-(?P<slug>[a-z0-9\-]+)/(?P<page>\d+)$', 'view_article', name="blog_article"),
)
