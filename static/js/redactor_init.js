/*
 * BluStone
 * http://www.blustone.fr/
 */

$(function() {

    var admin_targets = $("#id_content");

    admin_targets.wrap("<div class=\"redactor-admin\"></div>");
    $(".redactor-admin textarea").redactor({
        imageUpload: "/components/upload-images/",
        imageGetJson: "/components/get-images/",
    });

});
