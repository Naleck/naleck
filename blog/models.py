from django.db import models


class Article(models.Model):
    """Article Model"""

    title = models.CharField(max_length=256, verbose_name='Titre')
    author = models.CharField(max_length=100, verbose_name='Auteur')
    content = models.TextField(verbose_name='Contenu')
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='Publié le')
    slug = models.SlugField(max_length=100)
    visible = models.BooleanField(default=True, verbose_name='Visibilité')
    enable_comments = models.BooleanField(default=True, verbose_name='Commentaires')
    category = models.ForeignKey('Category', verbose_name='Catégorie')

    class Meta:
        verbose_name = 'article'
        verbose_name_plural = 'articles'

    def __str__(self):
        return self.title

    def get_nb_comments(self):
        return Comment.objects.filter(article=self).count()


class Category(models.Model):
    """Categories model"""

    name = models.CharField(max_length=120, verbose_name='Nom')

    class Meta:
        verbose_name = 'catégorie'
        verbose_name_plural = 'catégories'

    def __str__(self):
        return self.name


class Comment(models.Model):
    """Comments model"""

    author = models.CharField(max_length=50, verbose_name='Auteur')
    email = models.EmailField(verbose_name='Adresse email')
    content = models.TextField(verbose_name='Commentaire')
    date = models.DateTimeField(auto_now_add=True)
    article = models.ForeignKey('Article')

    class Meta:
        verbose_name = 'commentaire'
        verbose_name_plural = 'commentaires'

