from django.conf.urls import patterns, url
from django.views.generic import TemplateView

urlpatterns = patterns(
    'home.views',
    url(r'^$', TemplateView.as_view(template_name='home/index.html'), name="home_index")
)